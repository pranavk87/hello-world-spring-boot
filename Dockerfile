FROM openjdk:latest
ADD target/HelloWorldSpringBoot-0.0.1-SNAPSHOT.jar hello-world-docker-app.jar
ENTRYPOINT [ "java", "-jar","hello-world-docker-app.jar" ]
EXPOSE 8080